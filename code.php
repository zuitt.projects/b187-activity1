
<?php


function getFullAddress($country, $city, $province, $specificAddress){
    return "$specificAddress, $city, $province, $country";
}

//getLetterGrade

// function determineTyphoonIntensity($windSpeed){
//     if($windSpeed < 30){
//         return "Not a typhoon yet";
//     }else if($windSpeed <= 61){
//         return "Tropical depression detected.";
//     }else if($windSpeed >= 62 && $windSpeed <= 88){
//         return "Tropical Strom detected";
//     }else if($windSpeed >= 89 && $windSpeed <= 117){
//         return "Severe tropical storm detected";
//     }else{
//         return "Typhoon detected";
//     }
// }

function getLetterGrade($grade){
    if($grade >= 98 && $grade <= 100){
        return "$grade is equavalent to A+";
    }else if($grade >= 95 && $grade <=97){
        return "$grade is equivalent to A";
    }else if($grade >= 92 && $grade <= 94){
        return "$grade is equivalent of A-";
    }else if($grade >= 89 && $grade <= 91){
        return "$grade is equivalent of B+";
    }else if($grade >= 86 && $grade <= 88){
        return "$grade is equivalent of B";
    }else if($grade >= 83 && $grade <= 85){
        return "$grade is equivalent of B-";
    }else if($grade >= 80 && $grade <= 82){
        return "$grade is equivalent of C+";
    }else if($grade >= 77 && $grade <= 79){
        return "$grade is equivalent of C";
    }else if($grade >= 75 && $grade <= 76){
        return "$grade is equivalent of C-";
    }else{
        return "$grade is equivalent of D";
    }
}


<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 1</title>
</head>
<body>
<h1>Full Address</h1>
    <p><?php echo getFullAddress("Philipines", "Metro Manila", "Quezon City", "3F Caswynn Bldg., Timog Avenue"); ?></p>

    <p><?php echo getFullAddress("Philipines", "Metro Manila", "Makati City", "3F Enzo Bldg., Buendia Avenue"); ?></p>

    <h1>Letter-Based Grading</h1>
    <p><?php echo getLetterGrade(87); ?></p>
    <p><?php echo getLetterGrade(94); ?></p>
    <p><?php echo getLetterGrade(74); ?></p>
</body>
</html>